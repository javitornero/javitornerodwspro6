<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
    include_once("controller/ControlUsuario.php");
?>
<html lang="es">
<?php include_once('web/head.php'); ?>
<body>
	<div class="divCabecera">
			<?php
				include_once('web/divLogo.php');
				include_once('web/menu.php');
			?>
	</div>
	<div id="contenedor">
		<div class="colOpciones"><?php include_once('web/divMod.php'); ?></div>
		<div class="contenido">
			<?php

			$controladorU= new ControlUsuario();

			//**********************************
			// Funciones para el CREATE

			if (isset($_REQUEST["sbCreateU"])) {
				$id = recoge("id");
				$nom = recoge("nom");
				$id_loc = recoge("id_loc");
				if ($id!="" && $nom!="" && $id_loc!="") {
					echo $controladorU->createU($id, $nom, $id_loc);
				} else {
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			}

			//**********************************
			// Funciones para el READ

			// Función que pinta una tabla con una vista de los objetos Usuarios
			function pintaTablaU($controladorU){
				$arrayObjs=$controladorU->readU();
				echo "<table>";
				echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Localización</td></tr>";
				if (is_array($arrayObjs)) { // Si es un array nos llegan todas los usuarios y los pintamos
					$row=count($arrayObjs);
					for($i=0;$i<$row;$i++){
						echo "<tr class='filaTabla'>";
						echo "<td class='colTabla'>".$arrayObjs[$i]->getId()."</td>";
						echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getNom()."</td>";
						echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getLocalizacion()->getNom()."</td>";
						echo "</tr>";
					}
				}else{ // Si no es un array es que devuelve un mensaje de error
					echo $controladorU->readU();
				}
				echo "</table>";
			}

			if (isset($_REQUEST["sbReadU"])) { ?>
				<div class="divReadRes" id="divReadU">
					<h1>Usuarios</h1>
					<div class="panelScroll">
						<?php pintaTablaU($controladorU); ?>
					</div>
				</div>
			<?php }

			//**********************************
			// Funciones para el UPDATE

			if (isset($_REQUEST["sbUpdateU"])) {
				$id = recoge("id");
				$nom = recoge("nom");	
				$id_loc = recoge("id_loc");
				if ($id!="" && $nom!="" && $id_loc!="") {				
					echo $controladorU->updateU($id, $nom, $id_loc);
				} else {				
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			}

			//**********************************
			// Funciones para el DELETE

			if (isset($_REQUEST["sbDeleteU"])) {
				$id = recoge("id");
				if ($id!="") {
					echo $controladorU->deleteU($id);
				} else {				
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			} 

			?>
		</div>
	</div>
	<footer>
		<?php include_once('web/pie.php'); ?>		
	</footer>
</body>
</html>