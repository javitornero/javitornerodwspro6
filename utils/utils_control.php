<?php

if (isset($_REQUEST['selectModelo'])) {
	if ($_REQUEST['selectModelo']=="fichero" || $_REQUEST['selectModelo']=="mysql") {
		$selectModelo=$_REQUEST['selectModelo'];
		include_once("./controller/Configuracion.php");
		$c1 = new Configuracion();
		$c1->set($selectModelo);
	}
}

function obtenerModelo() {
	if (Config::$modelo!="") {
		$tipo = Config::$modelo;
		switch ($tipo) {
			case 'fichero':
				include_once("./model/ModeloFichero.php");
				$modelo = new ModeloFichero();
				break;
			case 'mysql':
				include_once("./model/ModeloMysql.php");
				$modelo = new ModeloMysql(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
				break;
		}
		return $modelo;
	}
}    

// Función que limpia la entrada de datos en formularios
function recoge($campo) { 
	if (isset($_REQUEST[$campo])) {
		$valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
	} else {
		$valor = "";
	};
	return $valor;
}

?>