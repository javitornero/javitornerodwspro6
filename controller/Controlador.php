<?php
	include_once('utils/utils_control.php');
    include_once("./model/Localizacion.php");
/**
* 
*/
    class Controlador{

		function conectar(){
			$respuesta='OK';
			if (Config::$modelo=='mysql') {
				$modelo=obtenerModelo();
				$respuesta= $modelo->conectar();
			}
			return $respuesta;
		}

		function desconectar(){
			$modelo=obtenerModelo();
			return $modelo->desconectar();
		}

		function instalar(){
			$modelo=obtenerModelo();
			$respuesta= $modelo->instalar(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
			$respuesta= $respuesta. "<br><a class='aOK' href='index.php?selectModelo=".Config::$modelo."''>OK</a>";
			return $respuesta;
		}

		function newId($soporte){
			$modelo=obtenerModelo();
			return $modelo->newId($soporte);
		}

    }
?>