<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

    include_once("utils/utils_view.php");

    include_once("controller/Config.php");

    include_once("controller/Controlador.php");
    include_once("controller/ControlLocalizacion.php");
    include_once("controller/ControlUsuario.php");
    
	$controlador= new Controlador();
    $controladorLoc= new ControlLocalizacion();
    $controladorU= new ControlUsuario();

	$testConexion=$controlador->conectar();
?>
<html lang="es">
<?php include_once('web/head.php'); ?>
<body>
	<div class="divCabecera">
		<?php
			include_once('web/divLogo.php');
			if (Config::$modelo!="" && $testConexion=='OK') {
				include_once('web/menu.php');
			}
		?>
	</div>
	<div id="contenedor">
		<div class="colOpciones"><?php include_once('web/divMod.php'); ?></div>
		<div class="contenido">
			<?php
			if (Config::$modelo=="") { // Si no hay modelo activo lo pìdo y no doy mas opciones
					echo "Seleccione un modelo de entre las opciones del desplegable.";
			}else if ($testConexion!='OK') { // Si el test de conexión no nos devuelve OK ofrecemos instalación
				if (!isset($_REQUEST["instalar"])) { // Compueba si no existe la variable para instalar
					echo $testConexion; // Mostramos el mensaje de error SQL que nos devuelve el intento de conexión
					include_once('web/formInstalar.php'); // y mostramos el formulario que permite la instalación
				}else{
					echo $controlador->instalar(); // Si existe la variable para instalar nos devuelve mensaje de éxito o error
				}
			} else{ // Si ha llegado hasta aquí es porque se ha seleccionado un modelo válido y la prueba de conexión es OK
				if (Config::$modelo=="mysql") { $controlador->desconectar();} // Test de conexión OK, cerramos conexión a la BD
				if (isset($_GET["show"])) {
					switch ($_GET["show"]) {
					    case 'addLoc':
							include_once('templates/localizacionCreate.php');
						    break;
					    case 'showLoc':
							include_once('templates/localizacionRead.php');
					        break;
					    case 'upDateLoc':
							include_once('templates/localizacionUpdate.php');
					        break;
					    case 'delLoc':
							include_once('templates/localizacionDelete.php');
					        break;
					    case 'addU':
							include_once('templates/usuarioCreate.php');
					        break;
					    case 'showU':
							include_once('templates/usuarioRead.php');
					        break;
					    case 'upDateU':
							include_once('templates/usuarioUpdate.php');
					        break;
					    case 'delU':
							include_once('templates/usuarioDelete.php');
					        break;
					}
				}			
			}
			?>
		</div>
	</div>
	<footer>
		<?php include_once('web/pie.php'); ?>		
	</footer>
</body>
</html>