<div class="contenedorForm">
	<span class="cierraForm"><a class="aCierraForm" href="index.php?selectModelo=<?php echo Config::$modelo; ?>">&nbsp;x&nbsp;</a></span>
		<div class="divCRUD" id="divDelU">
		<h1>Eliminar usuario</h1>
		<form method="POST" action="vistaUsuario.php" >
			<input type="hidden" name="selectModelo" value="<?php echo Config::$modelo; ?>" />
			<table>
				<tr>
					<td class="tdCRUD">Seleccione usuario a eliminar: </td>
					<td class="tdCRUD">
						<select name="id" required >
							<?php pintaOptions($controladorU->readU()); ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD"><input type="submit" name="sbDeleteU" value="Eliminar"></td>
					<td class="tdCRUD"><input type="reset" name ="Borrar"></td>
				</tr>
			</table>
		</form>
	</div>
</div>