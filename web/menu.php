<nav class="menu">
    <ul class="menu--barra-principal">
        <li class="menu--barra-principal--entrada">
            <a href="#">Localizaciones</a>
            <ul class="submenu">
                <li id="addLoc" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=addLoc">Añadir</a></li>
                <li id="showLoc" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=showLoc">Mostrar</a></li>
                <li id="upDateLoc" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=upDateLoc">Actualizar</a></li>
                <li id="delLoc" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=delLoc">Eliminar</a></li>
            </ul>
        </li>
        <li class="menu--barra-principal--entrada">
            <a href="#">Usuarios</a>
            <ul class="submenu">
                <li id="addU" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=addU">Añadir</a></li>
                <li id="showU" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=showU">Mostrar</a></li>
                <li id="upDateU" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=upDateU">Actualizar</a></li>
                <li id="delU" class="submenu--entrada"><a href="index.php?selectModelo=<?php echo Config::$modelo; ?>&show=delU">Eliminar</a></li>
            </ul>
        </li>
    </ul> 
</nav>