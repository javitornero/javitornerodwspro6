<?php
	include_once("IModelo.php");
	include_once("Localizacion.php");
	include_once("Usuario.php");

/**
* 
*/
class ModeloMysql implements IModelo{
	protected $bdnombre;
	protected $bdusuario;
	protected $bdclave;
	protected $bdhostname;
	protected $conexion;

	function __construct($bdnombre, $bdusuario, $bdclave, $bdhostname) {
	   	$this->bdnombre=$bdnombre;
	   	$this->bdusuario=$bdusuario;
	   	$this->bdclave=$bdclave;
	   	$this->bdhostname=$bdhostname;
	}

	function conectar(){
		try {
			$bdconexion= new PDO('mysql:host='.$this->bdhostname.';dbname='.$this->bdnombre.';charset=utf8', $this->bdusuario, $this->bdclave);
			$bdconexion->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
		   	$this->conexion=$bdconexion;
			return "OK";
		} catch (PDOException $e) {
			return "<p>¡Error!: " . $e->getMessage() . "</p>";
		}
	}
	function desconectar(){
		$this->conexion= NULL;
	}

	function instalar($bdnombre, $bdusuario, $bdclave, $bdhostname){
		try {
			if (!$this->conexion) {
				$bdconexion= new PDO('mysql:host='.$bdhostname.';charset=utf8', $bdusuario, $bdclave); // No incluimos el nombre de la BD
				$bdconexion->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE); 
				if ($bdconexion->query(Config::$consultaCreaBD)) {
					$this->conectar();
					if ($this->conexion->query(Config::$consultaCreaTablas)) {
								$respuesta= "<p>Instalado con éxito.<p>\n";
					}else{
						$respuesta= "<p>Falló la creación de las tablas.<p>\n";
					}
				} else {
					$respuesta= "<p>Error al instalar.<p>\n";
				}
			} else {
				$respuesta= "No es necesario la reinstalación.";
			}
			return $respuesta;
		} catch (PDOException $e) {
			return "<p>¡Error!: " . $e->getMessage() . "</p>";
		}
	}

	// Función que devuelve el siguiente id. Siguiente id = id_máximo +1
	function newId($tabla){
		$this->conectar();
		$consulta="SELECT MAX(id) as maxId FROM $tabla";
		$result = $this->conexion->query($consulta);
		$res = $result->fetch(PDO::FETCH_ASSOC);
		$this->conexion = NULL;

		return $res["maxId"]+1; // Devolvemos valor máximo +1
	}


	//**********************************************************************
	//Create..

	// Función que recibe un objeto de la clase Localizacion y se vale de sus metodos get para obtener los datos y escribirlos en la tabla que corresponde
	function createLocalizacion($localizacion) {
		$this->conectar();
		$consulta = 'INSERT INTO localizaciones (id, nom) VALUES (:id, :nom)';
		$result = $this->conexion->prepare($consulta);
		$count = $result->execute(array(":id" => $localizacion->getId(),":nom"=>$localizacion->getNom()));
		if ($count == 1) {
			$respuesta="<p>Localización creada correctamente.</p>\n";
		} else {
			$respuesta="<p>No se ha podido crear la localización.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}

	// Función que recibe un objeto de la clase Usuario y se vale de sus metodos get para obtener os datos y escribirlos en la tabla que corresponde
	function createUsuario($usuario) {
		$this->conectar();
		$consulta = 'INSERT INTO usuarios (id, nom, id_loc) VALUES (:id, :nom, :id_loc)';
		$result = $this->conexion->prepare($consulta);
		$count = $result->execute(array(":id" => $usuario->getId(),":nom"=>$usuario->getNom(),":id_loc"=>$usuario->getLocalizacion()->getId()));
		if ($count == 1) {
			$respuesta="<p>Usuario creado correctamente.</p>\n";
		} else {
			$respuesta="<p>No se ha podido crear el usuario.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}


	//**********************************************************************
	//Read..

	// Devuelve array con todos los objetos de la clase Localizacion
	function readLocalizaciones(){
		$this->conectar();
		$consulta = "SELECT COUNT(*) FROM localizaciones";
		$result = $this->conexion->query($consulta); 
		if (!$result) { 
			return "<p>Error en la consulta.</p>\n"; 
		} else if ($result->fetchColumn() == 0) { 
			return "<p>La tabla no tiene registros todavía.</p>\n";
		}
		else 
		{
			$consulta = "SELECT * FROM localizaciones ORDER BY nom";
			$result = $this->conexion->query($consulta);
			if (!$result) {
				return "<p>Error en la consulta.</p>\n";
			} else {
				foreach ($result as $valor) {
					$arrayObj[]=new Localizacion($valor['id'], $valor['nom']);
				}
			}
			$this->conexion = NULL;
			return $arrayObj;
		}
	} 

	// Devuelve array con todos los objetos de la clase Usuario
	function readUsuarios(){
		$this->conectar();
		$consulta = "SELECT COUNT(*) FROM usuarios";
		$result = $this->conexion->query($consulta); 
		if (!$result) { 
			return "<p>Error en la consulta.</p>\n"; 
		} else if ($result->fetchColumn() == 0) { 
			return "<p>La tabla no tiene registros todavía.</p>\n";
		}
		else 
		{
			$consulta = "SELECT usuarios.id, usuarios.nom, localizaciones.id, localizaciones.nom FROM usuarios INNER JOIN localizaciones ON usuarios.id_loc = localizaciones.id ORDER BY usuarios.nom";
			$result = $this->conexion->query($consulta);
			if (!$result) {
				return "<p>Error en la consulta.</p>\n";
			} else {
				foreach ($result as $valor) {
					$arrayObj[]=new Usuario($valor[0], $valor[1], new Localizacion($valor[2],$valor[3]));
				}
			}
			$this->conexion = NULL;
			return $arrayObj;
		}
	}

	function countUsuariosByLoc($localizacion){
		$this->conectar();
		$consulta = "SELECT COUNT(*) FROM usuarios WHERE id_loc=:id";
		$result = $this->conexion->prepare($consulta);
		$result->execute(array(":id" => $localizacion->getId()));
		$number_of_rows = $result->fetchColumn();
		if (!$number_of_rows) {
			return 0; // Si la consulta falla o no hay registros que devuelva 0
		}else{			
			return $number_of_rows;
		}
	}

	//**********************************************************************
	//Update..

	function updateLocalizacion($localizacion){
		$this->conectar();
		$consulta = "UPDATE localizaciones SET nom=:nombre WHERE id=:id";
		$result = $this->conexion->prepare($consulta);
		if ($result->execute(array(":nombre" => $localizacion->getNom(),":id" => $localizacion->getId()))) {
			$respuesta="<p>Registro modificado correctamente.</p>\n";
		} else {
			$respuesta="<p>Error al modificar el registro.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}

	function updateUsuario($usuario){
		$this->conectar();
		$consulta = "UPDATE usuarios SET nom=:nombre, id_loc=:id_loc WHERE id=:id";
		$result = $this->conexion->prepare($consulta);
		if ($result->execute(array(":nombre" => $usuario->getNom(),":id_loc" => $usuario->getLocalizacion()->getId(),":id" => $usuario->getId()))) {
			$respuesta="<p>Registro modificado correctamente.</p>\n";
		} else {
			$respuesta="<p>Error al modificar el registro.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}


	//**********************************************************************
	//Create..

	function deleteLocalizacion($localizacion){
		$this->conectar();
		$consulta = "DELETE FROM localizaciones WHERE id=:indice";
		$result = $this->conexion->prepare($consulta);
		if ($result->execute(array(":indice" => $localizacion->getId()))) {
			$respuesta= "<p>Registro borrado correctamente.</p>\n";
		} else {
			$respuesta= "<p>Error al borrar el registro.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}

	function deleteUsuario($usuario){		
		$this->conectar();
		$consulta = "DELETE FROM usuarios WHERE id=:indice";
		$result = $this->conexion->prepare($consulta);
		if ($result->execute(array(":indice" => $usuario->getId()))) {
			$respuesta= "<p>Registro borrado correctamente.</p>\n";
		} else {
			$respuesta= "<p>Error al borrar el registro.</p>\n";
		}
		$this->conexion = NULL;

		return $respuesta;
	}
}

?>